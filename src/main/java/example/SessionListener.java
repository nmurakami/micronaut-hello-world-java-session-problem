package example;

import io.micronaut.runtime.event.annotation.EventListener;
import io.micronaut.session.event.SessionCreatedEvent;
import io.micronaut.session.event.SessionDeletedEvent;
import io.micronaut.session.event.SessionDestroyedEvent;
import io.micronaut.session.event.SessionExpiredEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

@Singleton
public class SessionListener {
    private static final Logger LOG = LoggerFactory.getLogger(SessionListener.class);

    @EventListener
    public void onSessionCreated(SessionCreatedEvent event) {
        LOG.info("Session created: " + event.getSource().getId() );
    }

    @EventListener
    public void onSessionDestroyedEvent(SessionDestroyedEvent event) {
        LOG.info("Session destroyed: " + event.getSource().getId() );
    }

    @EventListener
    public void onSessionExpiredEvent(SessionExpiredEvent event) {
        LOG.info("Session expired: " + event.getSource().getId() );
    }

    @EventListener
    public void onSessionDeletedEvent(SessionDeletedEvent event) {
        LOG.info("Session deleted: " + event.getSource().getId() );
    }
}
